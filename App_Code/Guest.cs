﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;

/// <summary>
/// Summary description for Guest
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class Guest : System.Web.Services.WebService {

    public Guest () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }



    [WebMethod(BufferResponse = true, EnableSession=true)]
    public string getGuest(string fName, string lName) {

        guestLookup.GuestsSoapClient guestClient = new guestLookup.GuestsSoapClient();
        string json = string.Empty;

        try
        {
            if (lName.Length < 1 && fName.Length < 1) {
                json = "";
                return json;
            }
            
            var result = guestClient.GetRoomGuestInformation(fName, lName);
            var reverse = result.OrderByDescending(x => x.ArrivalDate);
            json = JsonConvert.SerializeObject(reverse);
            return json;
        }

        catch (Exception e) {
            json = e.Message;
               return json;
        }
    }



    [WebMethod(BufferResponse = true, EnableSession = true)]
    public string getNewConferences() {

        guestLookup.GuestsSoapClient guestClient = new guestLookup.GuestsSoapClient();
        var result = guestClient.GetAllConferences("9DF3B529-54B6-49AD-8B6E-46F7BA0F737D");
        string json = JsonConvert.SerializeObject(result);
        return json;
    }





    [WebMethod(BufferResponse = true, EnableSession = true)]
    public string getConferenceDates(string conference) {
        guestLookup.GuestsSoapClient guestClient = new guestLookup.GuestsSoapClient();
        var result = guestClient.GetAllDatesForConference("9DF3B529-54B6-49AD-8B6E-46F7BA0F737D",conference);
        string json = JsonConvert.SerializeObject(result);
        return json;
    }





    [WebMethod(BufferResponse = true, EnableSession = true)]
    public string getCompleteItinerary(string conference, string date) {

        //DateTime dTime = Convert.ToDateTime(date);
        string json = string.Empty;
        guestLookup.GuestsSoapClient guestClient = new guestLookup.GuestsSoapClient();

        string[] words = date.Split('/');
        DateTime dtTest = new DateTime(Int32.Parse(words[2]), Int16.Parse(words[0]), Int16.Parse(words[1]));

        try
        {
            if (conference.Length < 1 || date.Length < 1 || conference == "Select..." || date == "Select...") {
                json = "";
                return json;
            }

            var result = guestClient.GetItineraryDetails(conference, dtTest);
            json = JsonConvert.SerializeObject(result);
            return json;
        }

        catch (Exception e) {
            json = e.Message;
            return json;
        }
    }
    
}
