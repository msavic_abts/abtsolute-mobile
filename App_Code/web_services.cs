﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Newtonsoft.Json;
using System.IO;
using System.Data;

/// <summary>
/// Summary description for web_services
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class web_services : System.Web.Services.WebService {

    public web_services () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(BufferResponse = true)]
    public xml_emp_local.Employees update() {
        
        // wsdl client prilikom generisanja na osnovu maticne klase automatski 
        // inkorporira u sebe i sve reference tj. metode za instanciranje ekternih xml struktura (xsd kalup), klasa itd,  
        // ako su  bar jednom instancirane u maticnoj klasi
        // prakticno u ovom slucaju inkorporira instancu xsd fajla sa servera
        // sto znaci da u sebi sadrzi i metode za instanciranje te iste xsd klase lokalno 

        //instanciranje default klase koja sadrzi moje metode na serveru
        xml_emp_local.xml_emp employees = new xml_emp_local.xml_emp();
        
        //klasa namespace-a xml_emp_local.cs, instanciranje Employees klase na osnovu xsd-a sa servera
        xml_emp_local.Employees emp = employees.updateEmployees();
        return emp;
    }

    
    // ABTSOLUTE MOBLIE - TESTING LOGIN ADN ADDRESS BOOK
    
    // LOGIN WEBMETHOD
    [WebMethod(BufferResponse = true, EnableSession=true)]
    public bool Login(string username, string pwd) {

        AddressBookService.AddressBook adrb = new AddressBookService.AddressBook();
        bool success = adrb.LoginUser(username, pwd);

        if (success)
        {
            HttpContext.Current.Session["user"] = username;
            return true;
        }

        else return false;
        
    }

    
    // GTE CURRENT USER
    [WebMethod(BufferResponse=true, EnableSession=true)]
    public string getCurrentUser() {

        string userCurrent;

        if (Session["cm"] != null) {
            userCurrent = Session["cm"].ToString();
            return userCurrent;
        }

        else if (Session["user"] != null) {
            userCurrent = Session["user"].ToString();
            return userCurrent;
        }

        else { 
            return ""; 
        }
    }

    // GET ADDRESS BOOK WEBMETHOD
    [WebMethod(BufferResponse = true, EnableSession = true)]
    public string getAddressBook(string cm)
    {
        
        // get the list
        if (Session["user"] != null)
        {
            // store to string for comparison
            /*string cmCurrent = Session["user"].ToString();*/
            
            // if theres data in session and user hasn`t changed do not make a call to web services
            /*if (Session["addressBook"] != null  && cmCurrent == cm) //- za sada se logujes sa Vlajkovim user-om, odkomentarisi ovo kad budes dobio do ane njen login
            {
                string sessionData = Session["addressBook"].ToString();
                return sessionData;
            }*/
            
            // if there`s no session data make a call to web service
            /*else
            {*/
                Session["cm"] = cm;
                String user = Session["user"].ToString();
                AddressBookService.AddressBook adrb = new AddressBookService.AddressBook();
                DataTable dt = adrb.GetAddressBook(cm, "", "", "", "");
                string result = serialize(dt);

                //store result to string for quicker access
                Session["addressBook"] = result;
                return result;
            /*}*/
        }
        // if no session return empty string
        else return "";
    }

    [WebMethod(BufferResponse = true, EnableSession = true)]
    public string ddlReload() {

        string result;

        if (Session["addressBook"] != null)
        {
            result = Session["addressBook"].ToString();
        }
        else {
            result = "";
        }
        return result;
    }

    // GET CONFERENCE MANAGER FROM SESSION
    [WebMethod(BufferResponse = true, EnableSession = true)]
    public string getCMFromSession() {
        
        if (Session["cm"] != null)
        {
            return Session["cm"].ToString();
        }

        else {
            return "nosession";
        }
    }


    [WebMethod(BufferResponse = true, EnableSession=true)]
    public string getFilters(string filter) {
        AddressBookService.AddressBook adrb = new AddressBookService.AddressBook();
        DataSet ds =  adrb.GetUsersAndCompanyTypes();
        DataTable dt1 = ds.Tables[0];
        DataTable dt2 = ds.Tables[1];

        string cm = serialize(dt1);
        string cType = serialize(dt2);

        if (filter == "cm")
        {
            return cm;
        }

        else return cType;
        
    }
    
    // METHODS USING CompanyInfoAndNotes.cs WSDL Client

    // GET COMPANY INFORMATION NOTES AND CONTACTS
    [WebMethod(BufferResponse = true, EnableSession = true)]
    public string getCompanynformation(string ID) {
        AddressBookService.Companies companies = new AddressBookService.Companies();
        AddressBookService.Company company = companies.GetCompanyDetails(ID);

        string json = JsonConvert.SerializeObject(company);
        return json;

    }

    //methods using Proposals.cs wsdl client

    //get all proposals
    [WebMethod(EnableSession = true, BufferResponse = true)]
    public string getProposals(string projectID)
    {

        // from proposals > extract one proposal (prop)
        AddressBookService.Proposals proposals = new AddressBookService.Proposals();

        //AddressBookService.Proposal prop = proposals.GetAllProposals(propID);
        List<AddressBookService.ProposalHeader> prop = proposals.GetAllProposals(projectID).ToList<AddressBookService.ProposalHeader>();

        //sort list by group name 
        List<AddressBookService.ProposalHeader> sortedList = prop.OrderBy(x => x.GroupName).ToList();

        //string json = JsonConvert.SerializeObject(proposals.GetAllProposals(projectID));
        string json = JsonConvert.SerializeObject(sortedList);
        HttpContext.Current.Session["proposals"] = json; // store to session
        return json;
        
    }

    // GET A LIST OF CURRENT CONFERENCES
    [WebMethod(EnableSession = true, BufferResponse = true)]
    public string getConferences() {
        AddressBookService.Proposals proposals = new AddressBookService.Proposals();
        string json = JsonConvert.SerializeObject(proposals.GetAllConferences());
        return json;
    }

    // GET PROPOSAL DETAILS
    [WebMethod(EnableSession=true, BufferResponse=true)]
    public string getProposalDetails(string IDProposal) {
        AddressBookService.Proposals proposals = new AddressBookService.Proposals();
        AddressBookService.Proposal proposal = proposals.GetProposalDetails(IDProposal);

        string json = JsonConvert.SerializeObject(proposal);
        return json;
    }
    


    //METHODS USING GuestListItinerary.cs wsdl client
    
    // GET GUEST LIST DETAILS
    [WebMethod(EnableSession = true, BufferResponse = true)]
    public string getGuestList(string IDProposal) {
        AddressBookService.GuestListItinerary gli = new AddressBookService.GuestListItinerary();

        List<AddressBookService.GuestList> gliList = gli.GetGuestListDetails(IDProposal).ToList<AddressBookService.GuestList>();
        
        IEnumerable<AddressBookService.GuestList> sortedGliList = 
            from gliElement in gliList 
            orderby gliElement.LastName ascending, 
            gliElement.FirstName ascending 
            select gliElement;

        //string json = JsonConvert.SerializeObject(gli.GetGuestListDetails(IDProposal));
        string json = JsonConvert.SerializeObject(sortedGliList);
        return json;
    }

    // GET ITINERARY
    [WebMethod(EnableSession = true, BufferResponse = true)]
    public string getItinerary(string IDProposal)
    {
        AddressBookService.GuestListItinerary gli = new AddressBookService.GuestListItinerary();
        string json = JsonConvert.SerializeObject(gli.GetItineraryDetails(IDProposal));
        return json;
    }

    // GET ITINERARY DETAILS
    [WebMethod(EnableSession = true, BufferResponse = true)]
    public string getItineraryDetails(string IDProposal)
    {
        AddressBookService.GuestListItinerary gli = new AddressBookService.GuestListItinerary();
        var itineraryList = gli.GetItineraryDetails(IDProposal);
        string json = JsonConvert.SerializeObject(itineraryList);
        return json;
    }

    // EDIT ITINERARY NOTE
    [WebMethod(EnableSession = true, BufferResponse = true)]
    public string editItineraryNote(long idItinerary, string note, string username, string authKey) {

        AddressBookService.GuestListItinerary gli = new AddressBookService.GuestListItinerary();
        bool success = gli.AddItineraryNote(idItinerary, note, username, authKey); // ovde si stao, sad moras po rucak a kad se vratis odradi interfejs na front endu i ajax post za ovu metodu
        
        return "";
    }

    // PROPOSAL SEARCH KEYWORD -> STORE TO SESSION
    [WebMethod(EnableSession = true, BufferResponse = true)]
    public void setSearchCriteria(string keyword) {
        Session["proposalKeyword"] = keyword;
    }
    
    // PROPOSAL SEARXH KEYWORD <- GET FROM SESSION
    [WebMethod(EnableSession = true, BufferResponse = true)]
    public string getSearchCriteria() {
        string keyword = Session["proposalKeyword"].ToString();
        return keyword;
    }

    // SERIALIZE DATATABLE METHOD
    public string serialize(DataTable dt) {
        string json = JsonConvert.SerializeObject(dt);
        return json;
    }
}
