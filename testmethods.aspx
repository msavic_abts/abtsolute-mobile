﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="testmethods.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Testing ABTSolute connection</title>
    <script src="Scripts/jquery-2.0.3.js" type="text/javascript"></script>

</head>

<body>
    <form id="form1" runat="server">
    <!-- TESTING USER CONTROL AND XML REMOTE AND LOCAL UPDATE -->
    <div style="display:none">
        <%--<uc1:test_user_control ID="test_user_control1" runat="server" />--%>
        <asp:Button runat="server" ID="btnShow" Text="Show Data" OnClick="btnShow_click" />
    </div>
    <!-- TESTING CONNECTION TO ABTSOLUTE, FETCHING ADDRESS BOOK DATA -->
    <div>
       <ucAbtsolute:address_book ID="address_book" runat="server" /> 
    </div>

    </form>
</body>
</html>
