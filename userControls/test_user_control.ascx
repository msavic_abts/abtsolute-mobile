﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="test_user_control.ascx.cs" Inherits="test_user_control" %>

<style type="text/css">
    body 
    {
        font-family:Calibri;
        }
</style>

<!-- remote xml -->
<asp:Label runat="server" ID="lblRemote" Text="Remote XML" Font-Bold="true" ForeColor="Red" ></asp:Label><br />
<asp:GridView runat="server" ID="gvEmployees" AutoGenerateColumns="true" CellPadding="4" ></asp:GridView><br />


<!-- local xml -->
<asp:Label runat="server" ID="lblLocal" Text="Local XML" Font-Bold="true" ForeColor="Red"></asp:Label><br />
<asp:GridView runat="server" ID="gvEmployeesLocal" AutoGenerateColumns="true" AutoGenerateEditButton="true" CellPadding="4"
    OnRowEditing="gvEmployees_editRow" 
    OnRowCancelingEdit="gvEmployees_cancelRow"
    OnRowUpdating="gvEmployees_update" >
</asp:GridView>

<br />

<asp:Button runat="server" ID="btnAdd" Text="Add Record" OnClick="add_employee" />