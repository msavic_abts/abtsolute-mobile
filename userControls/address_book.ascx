﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="address_book.ascx.cs" Inherits="userControls_address_book" %>

<script type="text/javascript">

    function js_login() {
        $.ajax({
            type: "POST",
            url: "http://localhost:5011/web_services.asmx/Login",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //crossDomain: true,
            success: function (data) {alert(data.d)},
            error: function (error, data) {alert("fail")}
        });
        //end $.ajax
    }
    //end send()

    function js_getAddressBook() {
        $.ajax({
            type: "POST",
            url: "http://localhost:5011/web_services.asmx/getAddressBook",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //crossDomain: true,
            success: function (data) {
                $('#ab_container').html(data.d);
                //alert(data.d);
                //var addressBook = data.d;
                //alert(result);
            },
            error: function (error, data) { alert("failed to display data") }
        });
        //end $.ajax
    }
    //end send()

</script>

<p>Test Address Book</p>
<input type="button" onclick="js_login()" value="Test login" />
<input type="button" onclick="js_getAddressBook()" value="Test address book" />

<p>Address Book Data:</p>
<div id="ab_container"></div>