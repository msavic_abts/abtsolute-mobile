﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class test_user_control : System.Web.UI.UserControl
{
    //remote xml stored to ds
    //local xml stored to ds2
    
    DataSet ds;
    DataSet ds2;
    string filePath;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            populateGridView();
        }
    }

    protected void gvEmployees_editRow(Object sender, GridViewEditEventArgs e) {
        gvEmployeesLocal.EditIndex = e.NewEditIndex;
        updateData();
    }

    protected void gvEmployees_update(Object sender, GridViewUpdateEventArgs e) {
        //get selected row index
        GridViewRow row = gvEmployeesLocal.Rows[e.RowIndex];
        //get selected row cell text
        string fName = ((TextBox)row.Cells[2].Controls[0]).Text;
        string lName = ((TextBox)row.Cells[3].Controls[0]).Text;
        string position = ((TextBox)row.Cells[4].Controls[0]).Text;
        //store cell value to session
        ds2 = (DataSet)Session["localDataset"];
        // store path to session for easier use
        filePath = (String)Session["sessionPath"];
        
        // replace dataset value 
        ds2.Tables["Employee"].Rows[e.RowIndex]["FirstName"] = fName;
        ds2.Tables[0].Rows[e.RowIndex]["LastName"] = lName;
        ds2.Tables[0].Rows[e.RowIndex]["Position"] = position;
        // write to xml
        ds2.WriteXml(filePath);
        //exit edit mode
        gvEmployeesLocal.EditIndex = -1;
        //refresh gridview (show remote data stored in refreshed dataset, not local)
        populateGridView();
    }

   protected void gvEmployees_cancelRow(Object sender, GridViewCancelEditEventArgs e) {
        e.Cancel = true;
        gvEmployeesLocal.EditIndex = -1;
        populateGridView();
    }

   // add xml element 
   protected void add_employee(object sender, EventArgs ev)
   {

       int rowCount = gvEmployeesLocal.Rows.Count - 1;

       //retrieve session data
       ds2 = (DataSet)Session["localDataset"];
       filePath = (String)Session["sessionPath"];

       //create new row and assign it to the exiting dataset
       DataRow dr = ds2.Tables["Employee"].NewRow();
       dr["ID"] = "118";
       dr["FirstName"] = "Grgur";
       dr["LastName"] = "Markovic";
       dr["Position"] = "Cook";

       //add to dataset
       ds2.Tables["Employee"].Rows.Add(dr);
       
       //write to xml
       ds2.WriteXml(filePath);
       populateGridView();
   }



    public void updateData() {
        populateGridView();
        updateXML();
    }

    public void populateGridView() {
        
        //Buduci da se na web service serveru instancira tipizirani dataset na osnovu xsd file-a
        //kad se prebaci kod klijenta ne mora de se kastuje vec se direktno upisuje u prazan genericki dataset.
        
        // WEB SERVICE SERVER
        // employees.xsd -> xml_emp.cs, metod: update_Employees()
        //-> 
        //WEB SERVICE CLIENT 
        //xml_emp_local.cs -> web_services.cs -> dataset OBJECT, metodi: update_Employees(), update()
                        
        web_services ws = new web_services();
        ds = ws.update();

        gvEmployees.DataSource = ds;
        gvEmployees.DataBind();

        Session["sessionData"] = ds;
        filePath = Server.MapPath("~/App_Data/employees_local.xml");
        Session["sessionPath"] = filePath;

        //show localXML in gridviewLocal
        //if dataset not yet populated
        if (ds2 == null)
        {
            ds2 = new DataSet();
            ds2.ReadXml(filePath);
        }
        
        //store latest dataset to session
        Session["localDataset"] = ds2;
        gvEmployeesLocal.DataSource = ds2;
        gvEmployeesLocal.DataBind();



    }

    public void updateXML() {

        //overwrite local copy of xml
        ds2 = (DataSet)Session["localDataset"];
        String employeesData = ds2.GetXml();
        ds2.WriteXml(filePath);
    }


    
}