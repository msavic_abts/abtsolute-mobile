﻿
var cinfo;

//on page_ci load
$(document).on('pageinit', '#page_ci', function (event) {

    // hide when the lists are empty 
    if ($('#companyContact li').length == 0) {
       $('#companyContact').hide();
    }

    if ($('#companyNotes li').length == 0) {
        $('#companyNotes').hide();
    }


    if ($(this).data("url") != null) {

        //first time load
        var query = $(this).data("url").split("?")[1];
        if (query != null) {
            var result = query.replace("id=", "");
            // get company info
            js_getCompany(result);
        }

        // reload page
        else {
            var query = window.location.href.split("?")[1];
            var result = query.replace("id=", "");
            // get company info
            js_getCompany(result);
        }
    }
    // error handler -> if no URL
    else {
        alert('Please relog.');
        js_goto('home.htm');
    }

    
});

function js_getCompany(id) {

        $.ajax({
            type: "POST",
            url: path + "/getCompanynformation",
            data: "{'ID': '" + id + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //crossDomain: true,
            beforeSend: function () {
                // get currently logged user
                //js_getCurrentUser();
            },
            success: function (data) {
                //alert(data.d);
                showCompanyInfo(data.d);
            },
            // case web service call fails
            error: function (error, data) { alert("failed to display conference information") }
        });
    //end $.ajax
  }
//end send()


function showCompanyInfo(data) {

    cinfo = JSON.parse(data);

    $('#companyInfo').html(
        '<li data-theme="b">Company Info</li><li><div style="overflow:hidden;white-space:normal; line-height:20px; font-size:16px;">' +
        //'<span>ID:</span> ' + cinfo.IDCompany + '<br />' +
        '<span style="color:navy;">' + cinfo.CompanyType + '</span><br />' +
        '<span style="font-weight:bold;">' + cinfo.Name + '</span><br /> ' +
        cinfo.Address + ', ' + cinfo.Zip + ', ' + cinfo.City + ',' + cinfo.Country + '<br /><br />' +
        //'<span style="color:navy;">Address 2:</span> ' + cinfo.Address2 + '<br />' +
        //'<span style="color:navy;">Zip:</span> ' + cinfo.Zip + '<br />' +
        '<span style="color:navy;">Url:</span> ' + cinfo.Url + '<br />' +
        '<span style="color:navy;">Email:</span> ' + cinfo.GeneralEmail + '<br />' +
        '<span style="color:navy;">Fax:</span> ' + cinfo.GeneralFax + '<br />' +
        //'<span style="color:navy;">Country:</span> ' + cinfo.Country + '<br />' +
        //'<span style="color:navy;">State:</span> ' + cinfo.State + '<br />' +
        //'<span style="color:navy;">Type:</span> ' + cinfo.CompanyType + '<br />' +
        /*'<span style="color:navy;">City:</span> ' + cinfo.City + */'</div><br /></li>' + '<li style="border-top:none;" data-icon="arrow-l"><a href="#" onclick="js_goto(\'adbook.htm\')">Back</a></li>');


    $('#companyInfo').show();
    // tweak for refreshing an element that hasn't been rendered yet
    $('#companyInfo').listview().listview('refresh');
}

function showContacts() {
    $('#companyNotes').html("");
    $('#companyNotes').hide();
    $('#companyContacts').show();

    for (i = 0; i < cinfo.Contacts.length; i++) {
        $('<li style="border-bottom:none;font-size:14px;"><span style="color:navy;">contact name:</span> ' + cinfo.Contacts[i].ContactName + '<br /><span style="color:navy;">title: </span> ' + cinfo.Contacts[i].Title + '<br /><span style="color:navy;">phone: </span> ' + cinfo.Contacts[i].Phone + '<br /><span style="color:navy;">fax: </span>' + cinfo.Contacts[i].Fax + '<br /><span style="color:navy;">email: </span>' + cinfo.Contacts[i].Email + '</li>').appendTo('#companyContacts');
    }
    $('#companyContacts').listview('refresh');
    //alert(cinfo.Contacts[2].Email);
}

function showNotes() {
    $('#companyContacts').html("");
    $('#companyContacts').hide();
    $('#companyNotes').show();

    for (i = 0; i < cinfo.Noteses.length; i++) {
        $('<li style="border-bottom:none;font-size:14px;"><a href="#' + i + '" style="font-size:14px;"><span style="color:navy;">conference: </span><b>' + cinfo.Noteses[i].ProjectName + '</b><br /><span style="color:navy;">created: </span> ' + cinfo.Noteses[i].DateAndCreator + '<br /><span style="color:navy;">follow up: </span> ' + cinfo.Noteses[i].FollowUpWhen + '<br /><span style="color:navy;">Interested In: </span>' + cinfo.Noteses[i].InterestedIn + '<br /><span style="color:navy;">contact: </span>' + cinfo.Noteses[i].Person + '<br /><br /><span class="noteText" style="color:red;font-size:12px; white-space:normal;">' + cinfo.Noteses[i].Note + '</span><br /></a></li>').appendTo('#companyNotes');
    }

    $('#companyNotes').listview('refresh');

}










