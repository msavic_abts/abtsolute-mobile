﻿// This file contains functions only, no onload method


// ID Proposal
var gli_ID;
var itinerary;
var guestList;

// method called from pInfo.htm onclick event
function js_getGuestList() {


    //parse url
    var query = window.location.href.split("?")[1];

    if (query != null) {
        gli_ID = query.replace("id=", "");
        }
        // error handler -> if no URL
    else {
        alert('Please relog.');
        js_goto('home.htm');
    }
    

    // getGuestList
    $.ajax({
        type: "POST",
        url: path + "/getGuestList",
        data: "{'IDProposal':'" + gli_ID + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.mobile.showPageLoadingMsg();
        },
        //crossDomain: true,
        success: function (data) {
            parseGuestList(data.d);
            $.mobile.hidePageLoadingMsg();
        },
        error: function (error, data) { alert("Guest list failed to load"); }
    });
    //end $.ajax
}
//end send()

// method called from pInfo.htm onclick event
function js_getItinerary() {

    if (gli_ID == null) {

        //parse url
        var query = window.location.href.split("?")[1];

        if (query != null) {
            gli_ID = query.replace("id=", "");
        }
            // error handler -> if no URL
        else {
            alert('Please relog.');
            js_goto('home.htm');
        }
    }

    // getGuestList
    $.ajax({
        type: "POST",
        url: path + "/getItineraryDetails",
        data: "{'IDProposal':'" + gli_ID + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.mobile.showPageLoadingMsg();
        },
        //crossDomain: true,
        success: function (data) {
            parseItinerary(data.d);
            $.mobile.hidePageLoadingMsg();
        },
        error: function (error, data) { alert("Itinerary failed to load"); }
    });
    //end $.ajax
}
//end send()

// method called from pInfo.htm onclick event
function js_showProposalDetails() {


    $('#guestList').css('display', 'none');
    $('#itinerary').css('display', 'none');
    $('#proposalItems').css('display', 'block');
    // the pipeline is located in getProposal.js
}




// invoked by js_getGuestList
function parseGuestList(data) {

    $('#guestList').html(''); // reset the list

    $('#proposalItems').css('display', 'none');
    $('#itinerary').css('display', 'none');
    $('#guestList').css('display', 'block');

    // $('#guestList').html("");

    // global var vendor is located at the top of getProposal.js and gets updated when loading 
    // and populating proposal items list within getProposal.js on pInfo.htm page-load
    
    guestList = JSON.parse(data);
    var myItem = getListItem(guestList); // get json item from the list based on a search criteria from the proposals.htm search field

    for (i = 0; i < guestList.length; i++) {

        var depDate = new Date(guestList[i].DepartureDate);
        var depDateString = depDate.toString();

        var arrDate = new Date(guestList[i].ArrivalDate);
        var arrDateString = arrDate.toString();

        var room;
        
        // check IDServiceItem for each guest list entry
        // by looping through array (table) of room/ID values
        // and checking which entry form that table corresponds to the 
        // IDServiceItem of the current guestlist entry
        for (j = 0; j < servceItemType_array.length; j++) {
            if (servceItemType_array[j][1] == guestList[i].IDServiceItem) {
                room = servceItemType_array[j][0];
            }
        }
        
        // put entered guest name on the top
        //if (typeof myItem !== 'undefined') {
        if (typeof myItem !== 'undefined' && guestList[i].GuestNumber == myItem.GuestNumber) {

                $('<li style="border-bottom:1px solid #ccc;font-size:12px;margin-bottom:10px;">' +
               '<span style="font-size:14px; font-weight:bold;color:#333;">' + room + '</span><br />' +
               '<span style="color:#000;">Guest ID: </span><span style="color:#37577b;">' + myItem.GuestNumber + '</span><br /><br />' +
               '<span style="color:#000;">Last Name: </span><span style="color:#37577b;"><b>' + myItem.LastName + '</b></span><br />' +
               '<span style="color:#000;">First Name: </span><span style="color:#37577b;"><b>' + myItem.FirstName + '</b></span><br />' +
               '<span style="color:#000;">Arrival Date: </span><span style="color:#FF0000;">' + arrDateString.substring(0, 10) + '</span><br />' +
               '<span style="color:#000;">Departure Date: </span><span style="color:#FF0000;">' + depDateString.substring(0, 10) + '</span><br />' +
               '<span style="color:#666;">' + myItem.Note +
               '</span></li>').prependTo('#guestList');

            //}

        } else { // all others in alphabetical order

            $('<li style="border-bottom:none;font-size:12px;">' +
                '<span style="font-size:14px; font-weight:bold;color:#333;">' + room + '</span><br />' +
                '<span style="color:#000;">Guest ID: </span><span style="color:#37577b;">' + guestList[i].GuestNumber + '</span><br /><br />' +
                '<span style="color:#000;">Last Name: </span><span style="color:#37577b;"><b>' + guestList[i].LastName + '</b></span><br />' +
                '<span style="color:#000;">First Name: </span><span style="color:#37577b;"><b>' + guestList[i].FirstName + '</b></span><br />' +
                '<span style="color:#000;">Arrival Date: </span><span style="color:#FF0000;">' + arrDateString.substring(0, 10) + '</span><br />' +
                '<span style="color:#000;">Departure Date: </span><span style="color:#FF0000;">' + depDateString.substring(0, 10) + '</span><br />' +
                '<span style="color:#666;">' + guestList[i].Note +
                '</span></li>').appendTo('#guestList');
        }
        }

        $('<li data-theme="b">' + vendor + '</li>').prependTo('#guestList');
        $('#guestList').listview('refresh');
    } 

// invoked by js_getItinerary()
function parseItinerary(data) {

    $('#proposalItems').css('display', 'none');
    $('#guestList').css('display', 'none');
    $('#itinerary').css('display', 'block');


    $('#itinerary').html("");
    itinerary = JSON.parse(data);

    if (itinerary.length < 1) {
        $('<li>No itinerary data available</li>').appendTo('#itinerary');
    }

    else {

        for (i = 0; i < itinerary.length; i++) {

            var date = new Date(itinerary[i].ForDate);
            var dateString = date.toString();
            var tmpNote = "";
            
            // check if there is no note
            if (itinerary[i].Note.length < 1) {
               tmpNote = " -";
            } else tmpNote = itinerary[i].Note;

            if (itinerary[i].IsConfirmed == false) {

                var itrDetails = "";
                var itrDetailsEsc = "";
                if (itinerary[i].ItineraryDetails.length > 0) {
                    for (item = 0; item < itinerary[i].ItineraryDetails.length; item++) {
                        itrDetails += itinerary[i].ItineraryDetails[item] + '<br />';
                    }
                    itrDetailsEsc = itrDetails.replace(/&/g, "and");
                }


                $('<li style="border-bottom:none;font-size:14px;"><a href="#" onclick="js_goto(\'itinerary.htm?date=' + dateString.substring(0, 10) +
                    '&vendor=' + itinerary[i].VendorName.replace(/'/g, "") +
                    '&service=' + itinerary[i].ServiceItemName.replace(/'/g, "") +
                    '&quantity=' + itinerary[i].Quantity + '&note=' + tmpNote.replace(/'/g, "") +
                    '&details=' + encodeURIComponent(itrDetailsEsc.replace(/'/g, "")) + '\');">' +
                    '<span style="font-size:12px; font-weight:bold;color:#FF0000;">' + dateString.substring(0, 10) + '</span><br />' +
                    '<span style="color:#000;">Vendor: </span><span style="text-decoration:line-through;color:#666;">' + itinerary[i].VendorName + '</span><br />' +
                    '<span style="color:#000;">Details: </span><span style="text-decoration:line-through;color:#666;white-space:normal;">' + itinerary[i].ServiceItemName + '</span><br />' +
                    '<span style="color:#000;">Qty: </span><span style="text-decoration:line-through;color:#666;">' + itinerary[i].Quantity + '</span><br />' +
                    '<span style="text-decoration:line-through;color:#666;">' + itinerary[i].Note +
                    '</span></a></li>').appendTo('#itinerary');
            }
            else {

                // store itinerary details to local var
                var itrDetails = "";
                var itrDetailsEsc = "";
                if (itinerary[i].ItineraryDetails.length > 0) {
                    for (k = 0; k < itinerary[i].ItineraryDetails.length; k++) {
                        itrDetails += itinerary[i].ItineraryDetails[k].FieldName + ': ' + itinerary[i].ItineraryDetails[k].Value + '<br />';
                    }
                    itrDetailsEsc = itrDetails.replace(/&/g, "and");
                }


                $('<li style="border-bottom:none;font-size:14px;"><a href="#" onclick="js_goto(\'itinerary.htm?date=' + dateString.substring(0, 10) +
                    '&vendor=' + itinerary[i].VendorName.replace(/'/g, "") +
                    '&service=' + itinerary[i].ServiceItemName.replace(/'/g, "") +
                    '&quantity=' + itinerary[i].Quantity +
                    '&note=' + tmpNote.replace(/'/g, "") +
                    '&details=' + encodeURIComponent(itrDetailsEsc.replace(/'/g, "")) + '\');"' +
                    '<span style="font-size:12px; font-weight:bold;color:#FF0000;">' + dateString.substring(0, 10) + '</span><br />' +
                    '<span style="color:#000;">Vendor: </span><span style="color:#2f456e;">' + itinerary[i].VendorName + '</span><br />' +
                    '<span style="color:#000;">Details: </span><span style="color:#2f456e;white-space:normal;">' + itinerary[i].ServiceItemName + '</span><br />' +
                    '<span style="color:#000;">Qty: </span><span style="color:#2f456e;">' + itinerary[i].Quantity + '</span><br />' +
                    '<span style="color:#2f456e;">' + itinerary[i].Note +
                    '</span></a></li>').appendTo('#itinerary');
            }

        }
    }

    $('#itinerary').listview('refresh');
}




// invoked sort the rooming list based on search criteria (search field in proposals.htm -> sort result in pinfo.htm
function getListItem(list) {

    for (var i = 0 ; i < list.length; i++) {
        var obj = list[i];
        var arrLn = obj["LastName"].toLowerCase(); // right now this sort option works only with Firstname/Lastname, it can be modified to utilize any array element
        var arrFn = obj["FirstName"].toLowerCase();
        //for (var j = 0; j < arr.length; j++) {
            //if (arr[j] == "Evangelou") {
        var foundLastName = arrLn.indexOf(searchCriteria.toLowerCase()); // searchCriteria is a global var @getProposal.js
        if (foundLastName !== -1) {
            return obj;
        } 
        var foundFirstName = arrFn.indexOf(searchCriteria.toLowerCase());
        if (foundFirstName !== -1 && foundLastName == -1) {
            return obj;
        }
    }
        //}
}





