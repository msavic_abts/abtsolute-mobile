﻿

// #UI note - showProposalInfo() hides proposal items list at the end


var searchCriteria;
var proposalInfo;
var vendor;

var cached_propInfo = "";
var cached_propInfoItems = "";


// array for coded table
var servceItemType_array = new Array();

//on page_ci load
$(document).on('pageinit', '#page_pi', function (event) {

    //get search criteria (if any) from previous page (proposals.htm)
    getKeyword();

    // check if there is cached data
    var getProposalJs_cache = checkForCachedProposalInfo();

    if ($(this).data("url") != null) {

        //first time load
        var query = $(this).data("url").split("?")[1];
        if (query != null) {
            var result = query.replace("id=", "");

            if (!getProposalJs_cache) { //check cache
                // get company info
                js_getProposalDetails(result);
            }
        }

            // reload page
        else {
            var query = window.location.href.split("?")[1];
            var result = query.replace("id=", "");

            if (!getProposalJs_cache) { //check cache
                // get company info
                js_getProposalDetails(result);
            }
        }
    }
        // error handler -> if no URL
    else {
        alert('Please relog.');
        js_goto('home.htm');
    }


});

// GET PROPOSAL DETAILS -> first method
function js_getProposalDetails(ID) {

    $.ajax({
        type: "POST",
        url: path + "/getProposalDetails",
        data: "{'IDProposal':'" + ID + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (data) {
            showProposalInfo(data.d);
        },
        error: function (error, data) { alert("proposal failed to load"); }
    });
    //end $.ajax
}
//end send()



function showProposalInfo(data) { // called by the first method (shows basic proposal details and populates and then hides only the items list, 
                                  //guest list and itinerary are populated in the getGuestListItinerary.js file)


    $('#proposalItems').html('<li data-theme="b">Proposal Items</li>');
    proposalInfo = JSON.parse(data);

    //show proposal Info
    $('#proposalInfo li').not('.ulTitle').html('<span style="font-ssssize:14px; color:444;">' + proposalInfo.ProjectName +
        '<br /><span style="font-size:14px; font-weight:bold; color:#204d77;">' + proposalInfo.GroupName + '</span>' +
        '<br /><br /><span style="font-size:16px;">' + proposalInfo.CompanyName +
        '</span><br /><span style="font-size:12px; color:#000; font-weight:bold;">' + proposalInfo.BookingNumber + 
        '</span><br /><span style="">created by: </span><span style="font-size:14px; color:#204d77;">' + proposalInfo.UserCreator);

    // add back button
    $('<li style="border-top:none;" data-icon="arrow-l"><a href="#" onclick="js_goto(\'proposal.htm\')">Back</a></li>').appendTo('#proposalInfo');

    // cache prop Info
    cached_propInfo = $('#proposalInfo').html(); // cache proposal Info
    $('#proposalInfo').listview('refresh'); // then refresh


    // and then list all proposal Items
    if (proposalInfo.ProposalItems.length > 0) {


        for (i = 0; i < proposalInfo.ProposalItems.length; i++) {

            // check proposal items and store proposalInfo.ProposalItems[i].VendorName to global var vendor, if item type == Room;
            if (proposalInfo.ProposalItems[i].ServiceTypeName == 'Room') {
                vendor = proposalInfo.ProposalItems[i].VendorName;
            }

            // stavi u array element dve vrednosti
            servceItemType_array[i] = new Array();
            servceItemType_array[i][0] = proposalInfo.ProposalItems[i].ServiceItemName;
            servceItemType_array[i][1] = proposalInfo.ProposalItems[i].IDServiceItem;


            $('<li id="propItem' + i + '" style="border-bottom:none;"><span style="font-size:12px;font-weight:bold;">' + proposalInfo.ProposalItems[i].ServiceTypeName +
                '</span><br /><span style="color:#204d77; font-size:16px; zfont-weight:bold;">' + proposalInfo.ProposalItems[i].VendorName +
                '</span><br /><span style="font-size:12px; color:#000;">' + proposalInfo.ProposalItems[i].ServiceItemName +
                '</span><br /><span style="font-size:12px; color:#000;">Contact: <b>' + proposalInfo.ContactPerson +
                '</b></span><br /><br /></li>').appendTo('#proposalItems');

            // divide total days into columns and create a table
            noOfColumns(proposalInfo.ProposalItems[i].ProposalItemDays.length, 5);

        }
    }

    // cache proposal info (rooming list, Itinerary and proposal items)
    cached_propInfoItems = $('#proposalItems').html();

    // and then refresh the list
    $('#proposalItems').listview('refresh');

    // populate on load, then hide (displayed on proposalitemdetailsoption form the navmenu in yellow on pInfo.htm)
    $('#proposalItems').css('display', 'none'); // hides the items list 
    //$('#proposalItems').hide();

}

function checkForCachedProposalInfo() {


    if (cached_propInfo.length > 0 && cached_propInfoItems.length > 0) {

        $.mobile.showPageLoadingMsg();

        $('#proposalInfo').html(cached_propInfo);
        $('#proposalItems').html(cached_propInfoItems);

        $('#proposalInfo').listview('refresh'); // basic prop info
        $('#proposalItems').listview('refresh'); // proposal items and tables

        $.mobile.hidePageLoadingMsg();
        return true;
    }
    else {
        return false;
    }
}

// create tables
function noOfColumns(x, y) {
    var div = Math.floor(x / y);
    var rem = x % y;
    var total;
    if (rem > 0) {
        total = div + 1;
    }
    else total = div;

    var high = 5;
    var low = 0;

    for (k = 0; k < total; k++) {

        // count columns, create a loop that counts the total num of columns including the last one which has fewer elements
        if (k == div) {

            // loop for the last column
            for (j = high - y; j < (parseInt(high - y) + parseInt(rem)) ; j++) {
                var date = new Date(proposalInfo.ProposalItems[i].ProposalItemDays[j].Date);
                var dateString = date.toString();
                $('<span style="font-size:12px; color:#3a6893; width:40px; padding:2px; padding-top:3px;display:block; float:left; border:1px solid #ccc;">' + dateString.substring(4, 10) + '</span>').appendTo('#propItem' + i);

            }

            $('<div style="clear:both; margin-bottom:0px;"></div>').appendTo('#propItem' + i);

            for (j = high - y; j < (parseInt(high - y) + parseInt(rem)) ; j++) {
                $('<span style="font-weight:bold; font-size:12px; color:#FF0000; width:40px; overflow:hidden; display:block; float:left; padding:2px;;border:1px solid #ccc; border-top:0px;"><b style="padding-left:10px;">' + proposalInfo.ProposalItems[i].ProposalItemDays[j].Quantity + '</b></span>').appendTo('#propItem' + i);
            }

        }
        else {

            // loop for all other columns (j can take values from 0 to 5, 5-10, 10,15, etc... as it is incremented by Y with each k loop, see below)
            for (j = low; j < high; j++) {
                var date = new Date(proposalInfo.ProposalItems[i].ProposalItemDays[j].Date);
                var dateString = date.toString();
                $('<span style="font-size:12px; color:#3a6893; width:40px; padding:2px; padding-top:3px;display:block; float:left; border:1px solid #ccc;">' + dateString.substring(4, 10) + '</span>').appendTo('#propItem' + i);
            }

            $('<div style="clear:both; margin-bottom:0px;"></div>').appendTo('#propItem' + i);

            for (j = low; j < high; j++) {
                $('<span style="font-weight:bold; font-size:12px; color:#FF0000; width:40px; overflow:hidden; display:block; float:left; padding:2px;;border:1px solid #ccc; border-top:0px;"><b style="padding-left:10px;">' + proposalInfo.ProposalItems[i].ProposalItemDays[j].Quantity + '</b></span>').appendTo('#propItem' + i);
            }

        }
        high = high + y;
        low = high - y;

        $('<div style="clear:both; margin-bottom:0px;"></div>').appendTo('#propItem' + i);

    }

}

// get search criteria from backend session for firstname/lastname
function getKeyword() {
    $.ajax({
        type: "POST",
        url: path + "/getSearchCriteria",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (data) {
            searchCriteria = data.d;
        },
        error: function (error, data) { /*alert("Failed to get search criteria.");*/ }
    });
}




