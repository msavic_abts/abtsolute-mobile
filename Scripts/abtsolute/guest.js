﻿
// dodaj globalnu varijablu koja drzi celu listu uda se ne ucitava svaki put opet, samo proveri prilikom loadovanja da li je ima ili ne

var guestsFound = '';

function js_getGuestInfo() {

    var fName = $('#iptFName').val();
    var lName = $('#iptLName').val();
    //var path2 = "https://www.roomhandler.com/abtsolutemobile/Guest.asmx/";
    var path2 = "http://localhost:105/Guest.asmx/";


    $.ajax({
        type: "POST",
        url: path2 + "/getGuest",
        data: "{'fName':'" + fName + "', 'lName':'" + lName + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        crossDomain: true,
        beforeSend: function () {
            $.mobile.showPageLoadingMsg();
        },
        success: function (data) {
            $.mobile.hidePageLoadingMsg();
            parseGuestData(data.d);
        },
        error: function (error) { alert("Guest data failed to load"); alert(error.responseText); }
    });
    //end $.ajax
}
//end send()


function parseGuestData(data) {

    $('#list_ShowGuest').html('');

    if (data == "[]") {
        $('<li>No guest by that name was found. Please check if you have entered a valid fist and last name.</li>').appendTo('#list_ShowGuest');
        
    }
    else {
        var jSon = JSON.parse(data);

        $('<li data-theme="b" style="font-size:14px;">Guest(s) found:</li>').appendTo('#list_ShowGuest');
        for (i = 0; i < jSon.length; i++) {
            $('<li style="border-bottom:none;"><a href="guestProposalDetails.htm?name=' + jSon[i].FirstName + ', ' + jSon[i].LastName + '&conference=' + jSon[i].Conference +
                '&hotel=' + jSon[i].Hotel +
                '&roomType=' + jSon[i].RoomType +
                '&arrival=' + jSon[i].ArrivalDate +
                '&departure=' + jSon[i].DepartureDate +
                '&guestNumber=' + jSon[i].GuestNumber +
                '&assignedTo=' + jSon[i].AssignedTo +
                '&client=' + jSon[i].Client +
                '&city=' + jSon[i].City +
                '&group=' + jSon[i].Group + '"><br /><span style="color:#253e7e;">' + jSon[i].FirstName + ',' + jSon[i].LastName +
                '</span><br /><span style="font-size:12px; color:#000;">Contact Person: </span><span style="color:#ff0000; font-size:12px;">' + jSon[i].ContactPerson + '<br />' +
                '</span><span style="font-size:12px; color:#000;">Phone: </span><span style="color:#ff0000; font-size:12px;">' + jSon[i].ContactPhone + '<br />' +
                '</span><span style="font-size:12px; color:#000;">Email: </span><span style="color:#ff0000; font-size:12px;">' + jSon[i].ContactEmail + '<br />' +
                //'</span><br /><span style="font-size:14px; white-space:pre-wrap;">' + jSon[i].Conference + ', ' + jSon[i].Hotel +
                //'</span><br /><b style="color:#253e7e;font-size:12px; white-space:pre-wrap;">' + jSon[i].RoomType +
                //'</b><br /><span style="font-size:12px;">Arrival Date:' + jSon[i].ArrivalDate +
                //'<br />Departure Date:' + jSon[i].DepartureDate +
                /*'<br />' + jSon[i].GuestNumber + */'</span></a></li>').appendTo('#list_ShowGuest');
        }

        guestsFound = $('#list_ShowGuest').html();
    }
    $('#list_ShowGuest').listview('refresh'); // refresh css
}

$(document).on('pageinit', '#page_guestLookup', function (event) {
 
    if (guestsFound.length > 2) {
        $('#list_ShowGuest').html(guestsFound);
        $('#list_ShowGuest').listview('refresh');
    }

});

