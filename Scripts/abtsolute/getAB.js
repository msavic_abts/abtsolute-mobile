﻿
var cachedCManagerName = "";
var cachedCManagerID = "";
var cachedAddressBook = "";


//on page_ab load
$(document).on('pageinit', '#page_ab', function (event) {

    // adbook_list
    $(".adbook_list").hide(); // hide list (& search field)


    checkForCachedAddressBook();

    // hide when the list is empty 
    if ($('.adbook_list ul li').length == 0) {
        //$('.adbook_list').hide();
    }
    // populate the users list (meethod listed in getCM.js)
    // this method also invokes the -> load currently logged user method

    js_getCMs('cm');
    js_ddlReload();

});





function js_getCMFromSession() {
    $.ajax({
        async: false,
        type: "POST",
        url: path + "/getCMFromSession",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#ddl_selectUser').val($("#ddl_selectUser option:contains('" + data.d + "')").val());
        },
        // case web service call fails
        error: function (error, data) { alert("Failed to load conference manager. Please select one from the list.") }
    });
    //end $.ajax
}

function js_ddlReload() {

    $.ajax({
        async: false,
        type: "POST",
        url: path + "/ddlReload",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != "") {
                // if there`s NO address book data for logged in user
                if (data.d == "[]") {
                    $('#list_ShowAB').html('<li>No data available for selected user.</li>');
                    $('#list_ShowAB').listview('refresh');
                }
                    // if there IS address book data for loggeed in user
                else {
                    // parse data and populate the list then refresh for jqm css
                    parseABData(data.d);
                    $('#list_ShowAB').listview('refresh');
                }
            }
                // if no session/login
            else {
                alert('Please reload the list to view address book.');
                //js_goto('home.htm#page_login');
            }
        },
        // case web service call fails
        error: function (error, data) { /*alert("failed to display conference list for selected user")*/ }
    });
    //end $.ajax
}


function js_getAddressBook() {

    
    $.ajax({
        //async: false,
        type: "POST",
        url: path + "/getAddressBook",
        data: "{'cm': '" + currentUser + "'}", /* var current user je globalno definisana pri vrhu u getCM.js i dobija vrednost prilikom ucitavanja stranice */
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        beforeSend: function () {

            $('.adbook_list').show(); //unhide the list once it starts loading
            $('#list_ShowAB').html('<li>Populating the list...</li>');
            $('#list_ShowAB').listview('refresh');
            $.mobile.showPageLoadingMsg();
            
            //display the "first" message

            //$('#list_ShowAB').html('<li>Fetching Address Book Data....</li>');

            // display the second messsage after 4 sec
            // only if the list has one element (it`s not yet populated with data
            // this is introduced since the list is populated via web services only 
            // the first time, each next iteration is pulled from session
            // so there`s no need for 8sec message

            myfunction();

            //function for the second 4sec message
            function myfunction() {
                setTimeout(function () {
                    // count list elements
                    $count = $('#list_ShowAB li').size();
                    // if there`s only one element after 4 seconds ("Fetching address Book data..." message in it)
                    if ($count == 1) {
                        $('#list_ShowAB').html('<li>Populating the list...</li>');
                        //refresh css
                        $('#list_ShowAB').listview('refresh');
                    }
                }, 4);

            }

            //refresh css
            $('#list_ShowAB').listview('refresh');
        },
        //upon completion
        success: function (data) {
            $.mobile.hidePageLoadingMsg();
            // check user session - (data is empty if there`s no session on web service server)
            if (data.d != "") {
                // if there`s NO address book data for logged in user
                if (data.d == "[]") {
                    $('#list_ShowAB').html('<li>No data available for selected user.</li>');
                    $(".adbook_list").show(); // show list
                    $('#list_ShowAB').listview('refresh');
                }
                // if there IS address book data for loggeed in user
                else {
                    // parse data and populate the list then refresh for jqm css
                    parseABData(data.d);
                    $(".adbook_list").show(); // show list
                    $('#list_ShowAB').listview('refresh');
                }
            }
            // if no session/login
            else {
                alert('Please login to view address book.');
                js_goto('home.htm#page_login');
            }
        },
        // case web service call fails
        error: function (error, data) { alert("failed to display data") }
    });
    //end $.ajax
}
//end send()


//PARSE ADDRESS BOOK DATA
function parseABData(data) {
    // split the address book string
    var columns = data.split('{');
    $('#list_ShowAB').html('');


    // loop through the array and append elements to ul
    for (i = 0; i < columns.length; i++) {

        if (i > 0) {

            // check for commas at the end of entries (i.e. ,",") -> failsafe check before parsing the column
            var currentColumn = columns[i].replace(',\",\"', '\",\"');

            // split rows into items (field name + field value) i.e. Company name, contact, etc..
            var item = currentColumn.split(',\"');
            var label = [];
            label.length = item.length;

            // split each item into label with field name and filed value using For loop
            for (j = 0; j < item.length; j++) {
                if (item[j] != null) {
                    label[j] = item[j].split(':');
                }
            }
            
            // remove quotes
            $Code = label[1][1].replace(/\"/g, "");
            $Company = label[2][1].replace(/\"/g, "");
            $Type = label[3][1].replace(/\"/g, "");
            $Contact = label[4][1].replace(/\"/g, "");
            $Country = label[5][1].replace(/\"/g, "");
            $Phone = label[9][1].replace(/\"/g, "");
            $Email = label[11][1].replace(/\"/g, "");

            // replace empty entries with n/a
            var listRow = new Array($Company, $Type, $Contact, $Country, $Phone, $Email);

            for (k = 0; k < listRow.length; k++) {
                if (listRow[k] == "") {
                    listRow[k] = "n/a";
                }
            }

            // append to list
            $('<li data-icon="arrow-r" style="border-bottom:0px;"><a href="#" onclick="js_goto(\'cinfo.htm?id=' + $Code + '\')" style="font-weight:normal; color:#004e7a;"><b style="color:#333;">' + $Code + '</b>: ' + $Company + ', ' + $Type + ', ' + $Country + '<p style="font-size:14px; margin-top:2px; color:#000;">' + $Contact + '<br />phone: ' + listRow[4] + ', email: ' + listRow[5] + '</p></a></li>').appendTo('#list_ShowAB');
            
        }
    }

    cachedSelectedConference = $('#ddl_selectUser option:selected').text(); // cache selected CM name
    cachedSelectedConferenceID = $('#ddl_selectUser option:selected').val(); // cache selected CM ID
    cachedAddressBook = $('#list_ShowAB').html();

}

function splitRows (columns) {
    // split rows into items (field name + field value) i.e. Company name, contact, etc..
    var item = columns[i].split(',\"');
    var label = [];
    label.length = item.length;
}


function setDropDownListUser() {
    currentUser = $('#ddl_selectUser').val();
}

function checkForCachedAddressBook() {
    if (cachedAddressBook.length > 0) {

        $.mobile.showPageLoadingMsg();
        $(".adbook_list").show(); // show list and search field
        $('<option value="' + cachedCManagerID + '" style="border-bottom:0px;">' + cachedCManagerName + '</option>').prependTo('#ddl_selectUser');

        $('#list_ShowAB').html(cachedAddressBook); // get cached conference proposals
        $('#list_ShowAB').listview('refresh'); // sad samo dodaj da se ucita prethodno odabrana konferencija
        $(".adbook_list").show(); // show list and search field
        $.mobile.hidePageLoadingMsg();
    }
}
