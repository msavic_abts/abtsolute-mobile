﻿var currentUser;

function js_getCMs(filter) {

    $.ajax({
        //async: false,
        type: "POST",
        url: path + "/getFilters",
        data: "{'filter': '" + filter + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        beforeSend: function () {
            // get currently logged user
            js_getCurrentUser();
        },
        success: function (data) {
            parseCMData(data.d);
        },
        // case web service call fails
        error: function (error, data) { alert("failed to display conference managers") }
    });
    //end $.ajax
}
//end send()

function js_getCurrentUser() {

    $.ajax({
        async: false,
        type: "POST",
        url: path + "/getCurrentUser",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (data) {
            currentUser = data.d;
        },
        // case web service call fails
        error: function (error, data) { alert("failed to display current user. Please Login."); }
    });
    //end $.ajax
}
//end send()


//PARSE ADDRESS BOOK DATA
function parseCMData(data) {
    // split the address book string
    var columns = data.split('{');
    $('#ddl_selectUser').html('');


    // loop through the array and append elements to ul
    for (i = 0; i < columns.length; i++) {

        if (i > 0) {

            // split rows into items (field name + field value) i.e. Company name, contact, etc..
            var item = columns[i].split(',\"');
            var label = [];
            label.length = item.length;

            // split each item into label with field name and filed value using For loop
            for (j = 0; j < item.length; j++) {
                if (item[j] != null) {
                    label[j] = item[j].split(':');
                }
            }

            // remove quotes
            $CMtemp = label[1][1].replace(/\"/g, "");
            $CM = $CMtemp.replace("},", "");

            // append to list
            $('<option value="' + $CM + '" style="border-bottom:0px;">' + $CM + '</option>').appendTo('#ddl_selectUser');
            $('#ddl_selectUser').selectmenu('refresh');
            $('#ddl_selectUser').val($("#ddl_selectUser option:contains('" + currentUser + "')").val());
        }
    }

}
