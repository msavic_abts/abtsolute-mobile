﻿
var itineraryConference = "";
var itineraryDate = "";

function js_getConferenceItineraryData() {

   

    //var path2 = "https://www.roomhandler.com/abtsolutemobile/Guest.asmx/";
    var path2 = "http://localhost:105/Guest.asmx/";

    js_getCompleteItinerary();

    function js_getCompleteItinerary() {

        $.ajax({
            type: "POST",
            url: path2 + "/getCompleteItinerary",
            data: "{'conference':'" + itineraryConference + "', 'date':'" + itineraryDate + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.mobile.showPageLoadingMsg();
            },
            //crossDomain: true,
            success: function (data) {
                $.mobile.hidePageLoadingMsg();
                parseCompleteItineraryData(data.d);
            },
            error: function (error, data) { alert("Itinerary data failed to load"); }
        });
        //end $.ajax
    }
    //end send()
}
//end send()



// GET CONFERENCES
function js_getNewConferences() {

    //var path2 = "https://www.roomhandler.com/abtsolutemobile/Guest.asmx/";
    var path2 = "http://localhost:105/Guest.asmx/";

    $.ajax({
        type: "POST",
        url: path2 + "/getNewConferences",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (data) {
            var conference = JSON.parse(data.d);
            for (i = 0; i < conference.length; i++) {
                $('<option>' + conference[i].Name + '</option>').appendTo('#ddl_selectConfName');
            }
            $('#ddl_selectConfName').selectmenu('refresh');
        },
        error: function (error, data) { alert("Conference list data failed to load"); }
    });
    //end $.ajax
}
//end send()



// GET CONFERENCE DATES
function js_getConferenceDates() {

    //var path2 = "https://www.roomhandler.com/abtsolutemobile/Guest.asmx/";
    var path2 = "http://localhost:105/Guest.asmx/";

    $.ajax({
        type: "POST",
        url: path2 + "/getConferenceDates",
        data: "{'conference':'" + itineraryConference + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (data) {
            var itineraryDates = JSON.parse(data.d);
            for (i = 0; i < itineraryDates.length; i++) {
                $('<option>' + itineraryDates[i] + '</option>').appendTo('#ddl_selectConfDate');
            }
            $('#ddl_selectConfDate').selectmenu('refresh');
        },
        error: function (error, data) { alert("Conference dates failed to load"); }
    });
    //end $.ajax
}
//end send()



function parseCompleteItineraryData(data) {

    var completeItineraryDetails;
    $('#list_ShowCompleteItinerary').html('');

    if (data.length < 1 || data == "[]") {
        $('<li>No itinerary for that date was found. Please check if you have entered valid conference name and date.</li>').appendTo('#list_ShowCompleteItinerary');
    }
    else {

        var jSon = JSON.parse(data);

        $('<li data-theme="b" style="font-size:14px;">Itinerary details for ' + jSon[0].ConferenceName + ':</li>').appendTo('#list_ShowCompleteItinerary');

        for (i = 0; i < jSon.length; i++) {

            completeItineraryDetails = "";

            for (j = 0; j < jSon[i].ItineraryDetails.length; j++) {
                completeItineraryDetails += '<span><b>' + jSon[i].ItineraryDetails[j].FieldName + ': </b>' + jSon[i].ItineraryDetails[j].Value + '</span><br />';
            }

            $('<li style="font-size:12px; border-bottom:none;">' +
                '<a href="#" id="showItineraryDetailsText' + i + '" onclick="toggleItineraryText(' + i + ');" style="font-size:12px;"><br />' +
                    '<span style="font-size:14px;">Time: </span><span style="font-size:14px; font-weight:bold; color:#ff0000;">' + jSon[i].StartTime + '</span><br />' +
                    '<span style="font-size:14px; color:#2C7A94; white-space:pre-wrap;">' + jSon[i].VendorName + '</span><br />' +
                    '<span style="color:#666;">' + jSon[i].ServiceType + '</span>: <span style="white-space:pre-wrap;color:#ff0000;">' + jSon[i].ServiceName + '</span><br /><br />' +
                    '<span style="font-size:14px; color:#666;">' + jSon[i].ClientName + '<br />' +
                    '<b>' + jSon[i].GroupName + '</b></span><br />' +
                '</a>' +
                '<div id="itineraryDetailsText' + i + '" style="white-space:pre-wrap;font-size:12px; color:#2C7A94;padding:20px;display:none;">' + completeItineraryDetails + '</div>' +
                '<br /></li>').appendTo('#list_ShowCompleteItinerary');

            /*for (j = 0; j < jSon[i].length; jSon++) {
                $(jSon[i].ItineraryDetails[2].Value + '<br />').appendTo('#completeItineraryDetils');
            }*/
        }

        
    }
    $('#list_ShowCompleteItinerary').listview('refresh'); // refresh css
    $('#completeItineraryDetils').listview('refresh');
}

$(document).on('pageinit', '#page_guestGetConferenceItinerary', function (event) {
 
    $('#ddl_selectConfName').html('<option>Select...</option>');
    $('#ddl_selectConfDate').html('<option>Select...</option>');

    js_getNewConferences();

    $('#ddl_selectConfName').change(function () {
        $('#ddl_selectConfDate').html('<option>Select...</option>');
        itineraryConference = $('#ddl_selectConfName').val();
        js_getConferenceDates();
        
        //alert('radi');
    })

    $('#ddl_selectConfDate').change(function () {
        itineraryDate = $('#ddl_selectConfDate').val();
    });
    
});

function toggleItineraryText(selected) {
    var flag = 0;

    if ($('#itineraryDetailsText' + selected).css('display') == 'none') {
        $('#itineraryDetailsText' + selected).css('display', 'block');
        //$('#showItineraryDetailsText' + selected).css('border-bottom', '1px solid #999;')
        flag = 1;
    }

    if (flag == 0) {
        $('#itineraryDetailsText' + selected).css('display', 'none');
        //$('#showItineraryDetailsText' + selected).css('border-bottom', 'none;')
    }

}



