﻿
var propIdBackup; // acquired before the page has switched as a part of the previous url, used for back button


//on page_itinerary load
$(document).on('pageinit', '#page_itinerary', function (event) {
   
    propIdBackup = getItineraryUrlParameter('id');
    setTimeout(
        function () {
            showItineraryInfo();
        }, 1000);
    

});


function showItineraryInfo() {


    var date = getItineraryUrlParameter('date');
    var vendor = getItineraryUrlParameter('vendor');
    var service = getItineraryUrlParameter('service');
    var quantity = getItineraryUrlParameter('quantity');
    var note = getItineraryUrlParameter('note');
    var details = getItineraryUrlParameter('details');
    if (details.length < 1) {
        details = "Itinerary details not available";
    }


    $('#itineraryInfo li').not('.ulTitle').html('<span style="font-size:14px; color:444;">' + vendor +
        '<br /><span style="font-size:14px; font-weight:bold; color:#204d77;">' + service + '</span>' +
        '<br /><br /><span style="font-size:16px;">' + date +
        '</span><br /><span style="font-size:12px; color:#000; font-weight:bold;">Qty: ' + quantity +
        '</span><br /><span style="">Note: </span><span style="font-size:14px; color:#204d77;">' + note);

    // itinerary details
    $('<li style="border-top:none;"><span style="font-size:14px;">' + details + '</span></li>').appendTo('#itineraryInfo');

    // add note textarea
    $('<li style="border-top:none;background:#eee;"><div data-role="fieldcontain">'
        + '<label for="taNote" style="font-size:14px;display:block;margin-bottom:5px;">Note:</label>'
        + '<textarea rows="6" name="taNote" id="taNote" style="width:100%; border:1px solid #ccc; background:#fff;font-family:Arial;font-size:14px;"></textarea>'
        + '</div>'
        + '<input data-theme="e" type="button" name="btnSubmit" value="Submit note" onclick="js_submitNote();" style="margin-top:10px; width:100%; font-size:14px; padding:10px; border:1px solid #ccc; background:#ddd; color:#333;" />'
        + '</li>').appendTo('#itineraryInfo');

    // add back button (shows itinerary list on back button)
    $('<li style="border-top:none;" data-icon="arrow-l"><a href="#" onclick="js_getItinerary(); js_goto(\'pInfo.htm?id=' + propIdBackup + '\')">Back</a></li>').appendTo('#itineraryInfo');

    // refresh
    $('#itineraryInfo').listview('refresh');


    

}

var getItineraryUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};




function js_submitNote() {
    
    $.ajax({
        
        type: "POST",
        url: path + "/editItineraryNote",
        data: "{'keyword':'" + query + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (data) {
        },
        error: function (error, data) { alert("Failed to set search criteria."); }

    });
}