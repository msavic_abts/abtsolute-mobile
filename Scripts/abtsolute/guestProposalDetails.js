﻿

$(document).on('pageinit', '#page_guestProposalDetails', function (event) {

    setTimeout(
        function () {
            var name = getUrlParameter('name');
            var conference = getUrlParameter('conference');
            var hotel = getUrlParameter('hotel');
            var roomType = getUrlParameter('roomType');
            var arrival = getUrlParameter('arrival');
            var departure = getUrlParameter('departure');
            var guestNumber = getUrlParameter('guestNumber');
            var assignedTo = getUrlParameter('assignedTo');
            var client = getUrlParameter('client');
            var city = getUrlParameter('city');
            var group = getUrlParameter('group');
            populateGuestDetailsList(name, conference, hotel, roomType, arrival, departure, guestNumber, assignedTo, client, city, group);
        }, 1000);


});

function populateGuestDetailsList(name, conference, hotel, roomType, arrival, departure, guestNumber, assignedTo, client, city, group) {
    $('<li style="font-size:12px;"><br /><b>' + guestNumber +
        '</b><br /><br /><span style="font-size:14px;color:#206880;"> ' + name +
        '</span><br /><span style="font-size:14px; color:#ff0000;">' + group +
        '</span><br /><span style="font-size:14px;color:#666666;">' + client + '</span>' +
        '<br /><br /><span style="font-size:14px;">' + conference +
        '</span><br /><span style="font-size:14px;color:#206880;">' + hotel +
        '</span><br /><b>' + roomType +
        '</b><br /><br />arrival: <b>' + arrival +
        '</b><br />departure: <b>' + departure +
        '</b><br />assigned to: <b>' + assignedTo +
        '</b><br />city: <b>' + city +
        '</b><br /><br /></li>').appendTo('#ul_GPD');
    $('<li><a href="#" data-icon="arrow-l" onclick="checkSession(\'guest.htm\');">Back</a></li>').appendTo('#ul_GPD');

    $('#ul_GPD').listview('refresh');

}