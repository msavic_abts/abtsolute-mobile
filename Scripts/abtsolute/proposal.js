﻿
var query;
var cachedConferenceProposalData = "";
var cachedSelectedConference = "";
var cachedSelectedConferenceID = "";

//on page_ab load
$(document).on('pageinit', '#page_prop', function (event) {
    
    $(".proposal_list").hide(); // hide list and search field
    checkForCachedConferenceProposals(); // check for cached proposal list

    // reset previously stored proposal info and proposalID used on the next page as to prevent loading the previously cached proposal data
    cached_propInfo = "";
    cached_propInfoItems = "";
    gli_ID = null;


    // hide when the list is empty 
    if ($('.adbook_list ul li').length == 0) {
        //$('.adbook_list').hide();
        
       
    }
    // populate the users list (meethod listed in getCM.js)
    // this method also invokes the -> load currently looged user method

    //js_getCMs('cm');
    //js_ddlReload();

    js_getConferenceList();

    // get input filed search criteria
    $('.ui-input-text').on("input", function () {
        query = this.value;
    });

});




var conferences;
var proposalList;

function js_getConferenceList() {

    $.ajax({
        type: "POST",
        url: path + "/getConferences",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        beforeSend: function () {
            $.mobile.showPageLoadingMsg();
        },
        success: function (data) {
            ddlPopulateConference(data.d);
            $.mobile.hidePageLoadingMsg();
        },
        error: function (error, data) { alert("Conference list failed to load. Please try later."); }
    });
    //end $.ajax
}
//end send()

function ddlPopulateConference(data) {

    conferences = JSON.parse(data);
    var confReverse = conferences.reverse();

    for (i = 0; i < conferences.length; i++) {


        // CONFERENCE LIST FILTER - CURRENTLY SET TO SHOW ONLY 2014 CONFERENCES
        //if (conferences[i].ConferenceName.indexOf('2014') > -1) {
            $('<option value="' + conferences[i].IDConference + '" style="border-bottom:0px;">' + conferences[i].ConferenceName + '</option>').appendTo('#ddl_selectConf');
        //}
    }

    $('#ddl_selectConf').selectmenu('refresh');
    
}


function js_getProposal() {

    selectedConferenceID = $('#ddl_selectConf').val();

    $.ajax({
        //async: false,
        type: "POST",
        url: path + "/getProposals",
        data: "{'projectID':'" + selectedConferenceID + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".proposal_list").show(); // show list and search field
            $('#list_ShowProps').html('<li>Populating the list...</li>');
            $('#list_ShowProps').listview('refresh');
            $.mobile.showPageLoadingMsg();
        },
        //crossDomain: true,
        success: function (data) {
            if (data.d != '[]') {
                $(".proposal_list").show(); // show list and search field
                // method is at the bootm of the page
                ddlPopulateProposals(data.d);
                $.mobile.hidePageLoadingMsg();
                
            }
            else {
                $(".proposal_list").show(); // show list and search field
                $.mobile.hidePageLoadingMsg();
                $('#list_ShowProps').html('<li>No proposals found for this conference</li>');
                $('#list_ShowProps').listview('refresh');
            }
        },
        error: function (error, data) { alert("proposal list failed to load"); }
    });
    //end $.ajax
}
//end send()

function ddlPopulateProposals(data) {

    // check if there is at least one confirmed proposal
    var confirmed = false;

    // POPULATE PROPOSAL LIST ON proposals.htm page
    $('#list_ShowProps').html('');
    proposalList = JSON.parse(data);

    // List filter set to confirmed proposals
    for (i = 0; i < proposalList.length; i++) {
        if (proposalList[i].ProposalStatus == 'Confirmed') {

            confirmed = true; // there is at least one confirmed proposal

            $('<li style="border-bottom:none;" id="' +
                proposalList[i].BookingNumber + '"><a href="#" id="' + proposalList[i].IDProposal + '"onclick="postKeyword(); js_goto(\'pinfo.htm?id=' + proposalList[i].IDProposal + '\')">' +
                '<span style="font-size:14px; font-weight:bold; color:#204d77;white-space:normal;">' + proposalList[i].GroupName + '</span><br />' +
                '<span style="white-space:normal;">' + proposalList[i].CustomerName + '</span><br />' +
                '<span style="font-size:12px; color:#000; font-weight:bold;">' + proposalList[i].BookingNumber + '</span><br />' +
                '<span style="font-size:14px;color:#666;">assigned to: </span><span style="font-size:14px; color:#204d77;">' + proposalList[i].UserAssigned + '</span><br />' +
                '<span style="font-size:14px;color:#666;">status: </span><span style="font-size:14px; color:#204d77;">' + proposalList[i].ProposalStatus +
                '</span></a></li>').appendTo('#list_ShowProps');


            var prom = '#' + proposalList[i].IDProposal;

            var noGuests = proposalList[i].ProposalGuests.length;
            for (m = 0; m < noGuests; m++) {
                var name = '<span class="guestNames" style="visibility:hidden;">' + proposalList[i].ProposalGuests[m].FirstName + '' + proposalList[i].ProposalGuests[m].LastName + ', <span>';
                $(name).appendTo(prom);
            }

        } 
    }

    if (!confirmed) {
        $('#list_ShowProps').html('<li>No confirmed proposals found for this conference</li>');
    }

    cachedSelectedConference = $('#ddl_selectConf option:selected').text(); // cache selected conference
    cachedSelectedConferenceID = $('#ddl_selectConf option:selected').val(); // cache selected conference ID
    cachedConferenceProposalData = $('#list_ShowProps').html(); // cache conference proposals;

    $(".proposal_list").show(); // show list and search field
    $('#list_ShowProps').listview('refresh');
    //var testProp = proposalList[11].ProposalGuests.length;
}

function postKeyword() {
 
    /*var count = $('#list_ShowProps li div div a').length;
    for (i = 0; i < count; i++) {
        var oldJs = $('#list_ShowProps').find('.ui-link-inherit:eq(3)').attr('onclick');
        var newJs = oldJs.replace('undefined', query);
        var newClick = new Function(newJs);
        $('#list_ShowProps').find('.ui-link-inherit:eq(' + i + ')').attr('onclick', newJs);
    }*/

    $.ajax({
        type: "POST",
        url: path + "/setSearchCriteria",
        data: "{'keyword':'" + query + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (data) {
        },
        error: function (error, data) { alert("Failed to set search criteria."); }
    });
}

function checkForCachedConferenceProposals() {
    if (cachedConferenceProposalData.length > 0) {

        $.mobile.showPageLoadingMsg();
        $(".proposal_list").show(); // show list and search field
        $('<option value="' + cachedSelectedConferenceID + '" style="border-bottom:0px;">' + cachedSelectedConference + '</option>').prependTo('#ddl_selectConf');

        $('#list_ShowProps').html(cachedConferenceProposalData); // get cached conference proposals
        $('#list_ShowProps').listview('refresh'); // do not forget to load previously selected conference 
        $.mobile.hidePageLoadingMsg();
    }
}


